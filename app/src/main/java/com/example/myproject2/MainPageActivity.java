package com.example.myproject2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainPageActivity extends AppCompatActivity {
    Button LogOut;
    String EmailHolder;
    TextView Email;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        LogOut = (Button)findViewById(R.id.button1);
        Email = (TextView)findViewById(R.id.textView1);
        Intent intent =  getIntent();

        // Receiving User Email Send By MainActivity.
        EmailHolder = intent.getStringExtra(LoginActivity.UserEmail);

        // Setting up received email to TextView.
        Email.setText(Email.getText().toString()+EmailHolder);

        // Adding click listener to Log Out button.

        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//

                //Finishing current DashBoard activity on button click.
                finish();
                Toast.makeText(MainPageActivity.this,"Log Out Successfull", Toast.LENGTH_LONG).show();
            }
        });
    }
}
